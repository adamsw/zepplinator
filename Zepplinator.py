#!/usr/bin/env python

#initialisation
import math
import random
import pygame
from pygame.locals import *

pygame.init()

screen = pygame.display.set_mode((1680,1050),FULLSCREEN)
zepplinator = pygame.image.load('Zepplinator.png')
zepplinatordroite = pygame.image.load('ZepplinatorDroite.png')
canon_vertical = pygame.image.load('CanonVertical.png')
canon_droite = pygame.image.load('CanonDroit.png')
canon_gauche = pygame.image.load('CanonGauche.png')

boss1_vertical = pygame.image.load ('BossTank.png')
boss1_droite = pygame.image.load ('BossTankDroit.png')
boss1_gauche = pygame.image.load ('BossTankGauche.png')

boss2_vertical = pygame.image.load ('BossSniper.png')
boss2_droite = pygame.image.load ('BossSniperDroit.png')
boss2_gauche = pygame.image.load ('BossSniperGauche.png')

#boss3_vertical = pygame.image.load ('Boss.png')
#boss3_droite = pygame.image.load ('BossDroit.png')
#boss3_gauche = pygame.image.load ('BossGauche.png')

projectile = pygame.image.load("Projectile.png")
bomb = pygame.image.load("Ogive.png")
fond = pygame.image.load("fond2.png").convert()
explosion1 = pygame.image.load("Explosion1.png")
explosion2 = pygame.image.load("Explosion2.png")
explosion3 = pygame.image.load("Explosion3.png")
explosion4 = pygame.image.load("Explosion4.png")
explosion5 = pygame.image.load("Explosion5.png")
explosion6 = pygame.image.load("Explosion6.png")
explosion7 = pygame.image.load("Explosion7.png")
explosion8 = pygame.image.load("Explosion8.png")
sniper = pygame.image.load("BossSniper.png")

pausescreen = pygame.image.load("pause.png").convert_alpha()
pausescreen = pygame.transform.scale(pausescreen, (1680,1050))
gameoverscreen = pygame.image.load("gameoverscreen.png").convert_alpha()
gameoverscreen = pygame.transform.scale(gameoverscreen, (1680,1050))

pygame.mixer.music.load("Musique.mp3")
pygame.mixer.music.play(-1)
pygame.mixer.music.set_volume(0.2)

explosion1 = pygame.transform.scale(explosion1, (100,100))
explosion2 = pygame.transform.scale(explosion2, (100,100))
explosion3 = pygame.transform.scale(explosion3, (100,100))
explosion4 = pygame.transform.scale(explosion4, (100,100))
explosion5 = pygame.transform.scale(explosion5, (100,100))
explosion6 = pygame.transform.scale(explosion6, (100,100))
explosion7 = pygame.transform.scale(explosion7, (100,100))
explosion8 = pygame.transform.scale(explosion8, (100,100))
explosions = [explosion1, explosion2, explosion3, explosion4, explosion5, explosion6, explosion7, explosion8]
lieu_explo = []
stade_explo = []
temps_explo = []
suppr_explo = []

bomb = pygame.transform.scale(bomb, (90,90))
fond = pygame.transform.scale(fond, (1680,1050))


canon_vertical = pygame.transform.scale(canon_vertical, (250,250))
canon_droite = pygame.transform.scale(canon_droite, (250,250))
canon_gauche = pygame.transform.scale(canon_gauche, (250,250))

boss1_vertical = pygame.transform.scale(boss1_vertical, (300,300))
boss1_droite = pygame.transform.scale(boss1_droite, (300,300))
boss1_gauche = pygame.transform.scale(boss1_gauche, (300,300))

boss2_vertical = pygame.transform.scale(boss2_vertical, (300,300))
boss2_droite = pygame.transform.scale(boss2_droite, (300,300))
boss2_gauche = pygame.transform.scale(boss2_gauche, (300,300))

boss3_vertical = pygame.transform.scale(canon_vertical, (350,350))
boss3_droite = pygame.transform.scale(canon_droite, (350,350))
boss3_gauche = pygame.transform.scale(canon_gauche, (350,350))

projectile = pygame.transform.scale(projectile, (50,50))

ecriture1 = pygame.font.SysFont("arial", 16, True)
ecriture2 = pygame.font.SysFont("consolas", 150)

zepplinator = pygame.transform.scale(zepplinator, (250,250))
zepplinatordroite = pygame.transform.scale(zepplinatordroite, (250,250))

pygame.display.set_caption("Zepplinator")

points = 0
level = 2
augment = True
mini, maxi = (1, 2)
hp = 10
hpmax = 10.0
x = 10
y = 10
xb = 0
yb = 0

random1 = random.randrange(mini*60, maxi*60)
random2 = random.randrange(mini*60, maxi*60)
random3 = random.randrange(mini*60, maxi*60)
random4 = random.randrange(mini*60, maxi*60)
random5 = random.randrange(mini*60, maxi*60)
random6 = random.randrange(mini*60, maxi*60)
randomboss1 = random.randrange(60, 120)
randomboss3 = random.randrange(7, 15)

count1 = 0
count2 = 0
count3 = 0
count4 = 0
count5 = 0
count6 = 0
countboss1 = 0
countboss2 = 0
countboss3 = 0
boss3tir = 0
boss3fire = False

compte_niveau = 0

counts = [count1, count2, count3, count4, count5, count6]
randoms = [random1, random2, random3, random4, random5, random6]

direction = 0

bombing = False

running = True
clock = pygame.time.Clock()
count = 60

def drop():
   global xb, yb, bombing, count, bombing
   if count >= 60:
       bombing = True
       xb = x+80
       yb = y+150
       count = 0

class boss:
    def __init__(self,x, y, alive, genre):
        self.x = x
        self.y = y
        self.alive = alive
        self.pos = 2
        self.xtir = []
        self.ytir = []
        self.xajout = []
        self.yajout = []
        self.liste_suppression = []
        self.genre = genre
        if self.genre == "TANK":
            self.hp = 15
        else:
           self.hp = 6
           
        self.hpmax = float(self.hp)

    def draw(self):
        #dessine les boss
        global x, y
        if self.alive == True:
            if self.genre == "RATATATA":
               pygame.draw.rect(screen, (0,0,0), (self.x+155,self.y+320,60,10), 1)
               screen.fill((255,255,255), (self.x+156,self.y+321,58,8))
               screen.fill((0,255,0), (self.x+156, self.y+321, (self.hp/self.hpmax)*58, 8))
            else:
               pygame.draw.rect(screen, (0,0,0), (self.x+125,self.y+299,60,10), 1)
               screen.fill((255,255,255), (self.x+125,self.y+300,58,8))
               screen.fill((0,255,0), (self.x+125, self.y+300, (self.hp/self.hpmax)*58, 8))
            if self.genre == "TANK":
               if -200 < self.x-x < 200:
                  screen.blit(boss1_vertical, (self.x, self.y))
                  self.pos = 2
               elif self.x-x > 200:
                  screen.blit(boss1_gauche, (self.x, self.y))
                  self.pos = 1
               else :
                  screen.blit(boss1_droite, (self.x, self.y))
                  self.pos = 3
            elif self.genre == "SNIPER":
               if -200 < self.x-x < 200:
                  screen.blit(boss2_vertical, (self.x, self.y))
                  self.pos = 2
               elif self.x-x > 200:
                  screen.blit(boss2_gauche, (self.x, self.y))
                  self.pos = 1
               else :
                  screen.blit(boss2_droite, (self.x, self.y))
                  self.pos = 3
            else:
               if -200 < self.x-x < 200:
                  screen.blit(boss3_vertical, (self.x, self.y))
                  self.pos = 2
               elif self.x-x > 200:
                  screen.blit(boss3_gauche, (self.x, self.y))
                  self.pos = 1
               else:
                  screen.blit(boss3_droite, (self.x, self.y))
                  self.pos = 3
                  
        if len(self.xtir) > 0:
            for i in range (0, len(self.xtir)):
                screen.blit(projectile, (self.xtir[i], self.ytir[i]))
               
    def tir(self):
       
        #TANK
        global x, y
        if self.genre == "TANK":
            if self.alive == True:
                if self.pos == 1:
                    self.xtir.append(self.x + 75)
                    self.ytir.append(self.y + 0)
                    self.xajout.append((self.x + 75 - random.randrange(1,200)*math.cos(math.radians(random.randrange(1,360)))-x-125)/250)
                    self.yajout.append((self.y + 0 - random.randrange(1,200)*math.sin(math.radians(random.randrange(1,360)))-y-125)/250)
 
                elif self.pos == 2:
                    self.xtir.append(self.x + 120)
                    self.ytir.append(self.y + 0)
                    self.xajout.append((self.x + 120 - random.randrange(1,200)*math.cos(math.radians(random.randrange(1,360)))-x-125)/250)
                    self.yajout.append((self.y + 0 - random.randrange(1,200)*math.sin(math.radians(random.randrange(1,360)))-y-125)/250)

                else:
                    self.xtir.append(self.x + 175)
                    self.ytir.append(self.y + 0)
                    self.xajout.append((self.x + 175 - random.randrange(1,200)*math.cos(math.radians(random.randrange(1,360)))-x-125)/250)
                    self.yajout.append((self.y + 0 - random.randrange(1,200)*math.sin(math.radians(random.randrange(1,360)))-y-125)/250)

           #SNIPER
        elif self.genre == "SNIPER":
            if self.alive == True:
                
               if self.pos == 1:
                   self.xtir.append(self.x + 55)
                   self.ytir.append(self.y + 0)
                   self.xajout.append((self.x + 55 - random.randrange(1,50)*math.cos(math.radians(random.randrange(1,360)))-x-125)/60)
                   self.yajout.append((self.y + 0 - random.randrange(1,50)*math.sin(math.radians(random.randrange(1,360)))-y-125)/60)

               elif self.pos == 2:
                   self.xtir.append(self.x + 125)
                   self.ytir.append(self.y + 0)
                   self.xajout.append((self.x + 125 - random.randrange(1,50)*math.cos(math.radians(random.randrange(1,360)))-x-125)/60)
                   self.yajout.append((self.y + 0 - random.randrange(1,50)*math.sin(math.radians(random.randrange(1,360)))-y-125)/60)
   
               else:
                   self.xtir.append(self.x + 170)
                   self.ytir.append(self.y + 0)
                   self.xajout.append((self.x + 170 - random.randrange(1,50)*math.cos(math.radians(random.randrange(1,360)))-x-125)/60)
                   self.yajout.append((self.y + 0 - random.randrange(1,50)*math.sin(math.radians(random.randrange(1,360)))-y-125)/60)

         #RATATATA
        else:
            if self.alive == True:
                if self.pos == 1:
                    self.xtir.append(self.x + 115)
                    self.ytir.append(self.y + 90)
                    self.xajout.append((self.x + 120 - random.randrange(1,250)*math.cos(math.radians(random.randrange(1,360)))-x-125)/120)
                    self.yajout.append((self.y + 90 - random.randrange(1,250)*math.sin(math.radians(random.randrange(1,360)))-y-125)/120)

                elif self.pos == 2:
                   self.xtir.append(self.x + 155)
                   self.ytir.append(self.y + 80)
                   self.xajout.append((self.x + 160 - random.randrange(1,250)*math.cos(math.radians(random.randrange(1,360)))-x-125)/120)
                   self.yajout.append((self.y + 80 - random.randrange(1,250)*math.sin(math.radians(random.randrange(1,360)))-y-125)/120)

                else:
                   self.xtir.append(self.x + 200)
                   self.ytir.append(self.y + 85)
                   self.xajout.append((self.x + 200 - random.randrange(1,250)*math.cos(math.radians(random.randrange(1,360)))-x-125)/120)
                   self.yajout.append((self.y + 85 - random.randrange(1,250)*math.sin(math.radians(random.randrange(1,360)))-y-125)/120)


class canon:
   #création d'une classe pour les canons
   def __init__(self, x, y, alive):
       self.x = x
       self.y = y
       self.alive = alive
       self.pos = 2
       self.xtir = []
       self.ytir = []
       self.xajout = []
       self.yajout = []
       self.liste_suppression = []
       self.hp = 3
       self.hpmax = float(self.hp)
      
   def draw(self):
       #dessine les canons et les projectiles
       global x, y
       if self.alive == True:
           pygame.draw.rect(screen, (0,0,0), (self.x+100,self.y+220,60,10), 1)
           screen.fill((255,255,255), (self.x+101,self.y+221,58,8))
           screen.fill((0,255,0), (self.x+101, self.y+221, (self.hp/self.hpmax)*58, 8))
           if -200 < self.x-x < 200:
               screen.blit(canon_vertical, (self.x, self.y))
               self.pos = 2
           elif self.x-x > 200:
               screen.blit(canon_gauche, (self.x, self.y))
               self.pos = 1
           else :
               screen.blit(canon_droite, (self.x, self.y))
               self.pos = 3
       for i in range (0, len(self.xtir)):
           screen.blit(projectile, (self.xtir[i], self.ytir[i]))
      
      
      
   def tir(self):
       global x, y
       if self.alive == True:
           if self.pos == 1:
               self.xtir.append(self.x + 70)
               self.ytir.append(self.y + 50)
               self.xajout.append((self.x + 70 - random.randrange(1,200)*math.cos(math.radians(random.randrange(1,360)))-x-125)/180)
               self.yajout.append((self.y + 50 - random.randrange(1,200)*math.sin(math.radians(random.randrange(1,360)))-y-125)/180)

           elif self.pos == 2:
               self.xtir.append(self.x + 105)
               self.ytir.append(self.y + 50)
               self.xajout.append((self.x + 105 - random.randrange(1,200)*math.cos(math.radians(random.randrange(1,360)))-x-125)/180)
               self.yajout.append((self.y + 50 - random.randrange(1,200)*math.sin(math.radians(random.randrange(1,360)))-y-125)/180)

           else:
               self.xtir.append(self.x + 135)
               self.ytir.append(self.y + 50)
               self.xajout.append((self.x + 135 - random.randrange(1,200)*math.cos(math.radians(random.randrange(1,360)))-x-125)/180)
               self.yajout.append((self.y + 50 - random.randrange(1,200)*math.sin(math.radians(random.randrange(1,360)))-y-125)/180)



canon1 = canon(26, 595, False)
canon2 = canon(301, 595, False)
canon3 = canon(577, 595, False)
canon4 = canon(853, 595, False)
canon5 = canon(1129, 595, False)
canon6 = canon(1404, 595, False)

boss1 = boss(700, 560, False, "TANK")
boss2 = boss(700, 560, False, "SNIPER")
boss3 = boss(700, 505, False, "RATATATA")

liste_canons = [canon3, canon4, canon2, canon5, canon1, canon6]
liste_boss = [boss1, boss2, boss3]

def keytest():
   #Associe chaque clé à une action
   global direction, x, y
   pressed = pygame.key.get_pressed()
   if pressed[pygame.K_UP]:
       y -= 10
   if pressed[pygame.K_DOWN]:
       y += 10
   if pressed[pygame.K_RIGHT]:
       x += 10
       direction = 1
   if pressed[pygame.K_LEFT]:
       x -= 10
       direction = 0
   if pressed[pygame.K_SPACE]:
       drop()



def draw():
   #fonction de dessin
   global direction, x, y, xb, yb, hp, bombing, lieu_explo, stade_explo, temps_explo, points
   screen.blit(fond,(0,0))
   #rectangle hp
   screen.fill((255,255,255), (31, 31, 98, 28))
   pygame.draw.rect(screen, (0,0,0), (30,30,100,30), 1)
   if hp <0:
      hp = 0
   screen.fill((255-(hp/hpmax)*255,(hp/hpmax)*255,0), (31, 31, (hp/hpmax)*98, 28))   
   txt_hp = ecriture1.render("HP", True, (0,0,0))
   nb_hp = ecriture1.render(str(hp), True, (0,0,0))
   screen.blit(txt_hp,(140, 35))
   screen.blit(nb_hp,(75, 35))

   #rectangle bombe
   pygame.draw.rect(screen, (0,0,0), (1550,30,100,30), 1)
   screen.fill((255,255,255), (1551, 31, 98, 28))
   screen.fill((255-(count/60.0)*255,(count/60.0)*255,0), (1551, 31, (count/60.0)*98, 28))
   txt_bombe = ecriture1.render("Bombe:", True, (0,0,0))
   nb_bombe = ecriture1.render(str(round(count/60.0, 2))+" s", True, (0,0,0))
   screen.blit(txt_bombe,(1470, 35))
   screen.blit(nb_bombe,(1580, 35))
  
   txt_points = ecriture1.render("Points: "+str(points), True, (0,0,0))
   screen.blit(txt_points,(1000, 35))
  
   #test pour ne pas sortir de l'écran
   if x <= 0:
           x = 0
   if x >= 1420:
           x = 1420
   if y <= -50:
           y = -50
   if y >= 270:
           y = 270
   if direction == 0:
       screen.blit(zepplinator, (x,y))
   else:
       screen.blit(zepplinatordroite, (x,y))

   if bombing == True:
       yb += 20
       screen.blit(bomb,(xb,yb))
       if yb > 750:
           bombing = False
           lieu_explo.append((xb-5,yb-5))
           stade_explo.append(0)
           temps_explo.append(0)
           if canon1.alive:
              if canon1.x+50<xb+45<canon1.x+200:
                  canon1.hp -= 1
                  points += 10
           if canon2.alive:
              if canon2.x+50<xb+45<canon2.x+200:
                  canon2.hp -= 1
                  points += 10
           if canon3.alive:
              if canon3.x+50<xb+45<canon3.x+200:
                 canon3.hp -= 1
                 points += 10
           if canon4.alive:
              if canon4.x+50<xb+45<canon4.x+200:
                 canon4.hp -= 1
                 points += 10
           if canon5.alive:
              if canon5.x+50<xb+45<canon5.x+200:
                 canon5.hp -= 1
                 points += 10
           if canon6.alive:
              if canon6.x+50<xb+45<canon6.x+200:
                 canon6.hp -= 1
                 points += 10
           if boss1.alive:
              if boss1.x+20<xb+45<boss1.x+230:
                 boss1.hp -= 1
                 points += 50
           if boss2.alive:
              if boss1.x+45<xb+45<boss1.x+205:
                 boss2.hp -= 1
                 points += 50
           if boss3.alive:
              if boss1.x+30<xb+45<boss1.x+220:
                 boss3.hp -= 1
                 points += 50

   for i in liste_canons:
       i.draw()

   for i in liste_boss:
       i.draw()

def pause():
   global running
   paused = True
   pygame.mixer.music.fadeout(1500)
   r = pygame.mixer.music.get_pos()
   while paused:
       for event in pygame.event.get():
           if event.type == pygame.QUIT:
               pygame.quit()

           if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1 and 508<event.pos[0]<1173 and 345<event.pos[1]<494:
                   paused = False
                   pygame.mixer.music.play(-1, r/1000.0)
           elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1 and 665<event.pos[0]<999 and 525<event.pos[1]<674:
                   paused = False
                   running = False
                  
       screen.blit(pausescreen, (0,0))
       pygame.display.flip()

def gameover():
   global level, points, lost, hp, running
   pygame.mixer.music.fadeout(1500)
   points = 0
   while lost == True:
       for event in pygame.event.get():
           if event.type == pygame.QUIT:
               pygame.quit()

           if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1 and 508<event.pos[0]<1173 and 345<event.pos[1]<494:
               level = 1
               lost = False
               hp = 10
               pygame.mixer.music.play(-1)
               
               for i in liste_canons:
                   i.alive = False
                   i.xtir = []
                   i.ytir = []
                   i.xajout = []
                   i.yajout = []
                   i.liste_suppression = []
               for i in liste_boss:
                  i.alive = False
                  i.xtir = []
                  i.ytir = []
                  i.xajout = []
                  i.yajout = []
                  i.liste_suppression = []
                  
           elif event.type == pygame.MOUSEBUTTONDOWN and event.button ==1 and 665<event.pos[0]<999 and 525<event.pos[1]<674:
               lost = False
               running = False

       screen.blit(gameoverscreen, (0,0))
       pygame.display.flip()

  
def compteurs():
   global count, counts, randoms, countboss1, countboss2, countboss3, randomboss1, randomboss3, boss3fire, boss3tir
   countboss1 += 1
   countboss2 += 1
   countboss3 += 1
   if count<60:
       count += 1
   for i in range(0, 6):
       counts[i] +=1
   for i in range(0,6):
       if randoms[i] == counts[i]:
           counts[i]= 0
           randoms[i] = random.randrange(mini*60, maxi*60)
           liste_canons[i].tir()

   if countboss1 == randomboss1:
      boss1.tir()
      randomboss1 = random.randrange(60, 120)
      countboss1 = 0
   
   if countboss2 == 180:
      boss2.tir()
      countboss2 = 0

   if countboss3 == 150:
      boss3fire = True
      
   if boss3fire == True:
      boss3tir += 1
      if boss3tir == 15:
         randomboss3 -= 1
         boss3.tir()
         boss3tir = 0
         if randomboss3 == 0:
            boss3fire = False
            countboss3 = 0
            randomboss3 = random.randrange(7, 15)

   
def test_boss():
    global liste_boss, temps_explo, suppr_explo, lieu_explo, hp, points
    for k in liste_boss:
        if len(k.xtir)>0:
            for i in range (0, len(k.xtir)):
                if k.xtir[i]>1680 or k.xtir[i]<-30 or k.ytir[i]>1050 or k.ytir[i]<-30:
                    k.liste_suppression.append(i)
                elif direction == 1:
                    if x+10< k.xtir[i]+25<x+70 and y+90<k.ytir[i]+25<y+160 or x+70<k.xtir[i]+25<x+140 and y+50<k.ytir[i]+25<y+200 or x+140<k.xtir[i]+25<x+240 and y+80<k.ytir[i]+25<y+170:
                        if k.genre == "SNIPER" :
                            hp -= 5
                        elif k.genre == "TANK":
                            hp -= 2
                        else:
                            hp -= 1
                        points -= 10
                        k.liste_suppression.append(i)
                        lieu_explo.append((k.xtir[i]-25,k.ytir[i]-25))
                        stade_explo.append(0)
                        temps_explo.append(0)
                elif direction == 0:
                    if x+10<k.xtir[i]+25<x+110 and y+80<k.ytir[i]+25<y+170 or x+110<k.xtir[i]+25<x+180 and y+50<k.ytir[i]+25<y+200 or x+180<k.xtir[i]+25<x+240 and y+80<k.ytir[i]+25<y+170:
                        points -= 10
                        if k.genre == "SNIPER":
                            hp -= 5
                        elif k.genre == "TANK":
                            hp -= 2
                        else:
                            hp -= 1
                        k.liste_suppression.append(i)
                        lieu_explo.append((k.xtir[i]-25,k.ytir[i]-25))
                        stade_explo.append(0)
                        temps_explo.append(0)
                k.xtir[i] -= k.xajout[i]
                k.ytir[i] -= k.yajout[i]

            for i in range(0,len(k.liste_suppression)):
                k.liste_suppression[i] -= i

            for i in k.liste_suppression:
                k.xtir.pop(i)
                k.ytir.pop(i)
                k.xajout.pop(i)
                k.yajout.pop(i)

            k.liste_suppression = []

    for i in liste_boss:
        if i.hp == 0:
            i.alive = False

def test_canons():
   global liste_canons, temps_explo, suppr_explo, lieu_explo, hp, points
   for k in liste_canons:
       if len(k.xtir)>0:
           for i in range (0, len(k.xtir)):
               if k.xtir[i]>1680 or k.xtir[i]<-30 or k.ytir[i]>1050 or k.ytir[i]<-30:
                   k.liste_suppression.append(i)
               elif direction == 1:
                   if x+10< k.xtir[i]+25<x+70 and y+90<k.ytir[i]+25<y+160 or x+70<k.xtir[i]+25<x+140 and y+50<k.ytir[i]+25<y+200 or x+140<k.xtir[i]+25<x+240 and y+80<k.ytir[i]+25<y+170:
                       points -= 10
                       hp -= 1
                       k.liste_suppression.append(i)
                       lieu_explo.append((k.xtir[i]-25,k.ytir[i]-25))
                       stade_explo.append(0)
                       temps_explo.append(0)
               elif direction == 0:
                   if x+10<k.xtir[i]+25<x+110 and y+80<k.ytir[i]+25<y+170 or x+110<k.xtir[i]+25<x+180 and y+50<k.ytir[i]+25<y+200 or x+180<k.xtir[i]+25<x+240 and y+80<k.ytir[i]+25<y+170:
                       points -= 10
                       hp -= 1
                       k.liste_suppression.append(i)
                       lieu_explo.append((k.xtir[i]-25,k.ytir[i]-25))
                       stade_explo.append(0)
                       temps_explo.append(0)
               k.xtir[i] -= k.xajout[i]
               k.ytir[i] -= k.yajout[i]

           for i in range(0,len(k.liste_suppression)):
               k.liste_suppression[i] -= i

           for i in k.liste_suppression:
               k.xtir.pop(i)
               k.ytir.pop(i)
               k.xajout.pop(i)
               k.yajout.pop(i)

           k.liste_suppression = []

   for i in liste_canons:
       if i.hp == 0:
           i.alive = False

def test_explosions():
   global temps_explo, stade_explo, suppr_explo
   for i in range(0, len(lieu_explo)):
       screen.blit(explosions[stade_explo[i]], lieu_explo[i])
       temps_explo[i]+=1
       if temps_explo[i] >= 10:
           stade_explo[i]+=1
           temps_explo[i] = 0
       if stade_explo[i] == 8:
           suppr_explo.append(i)

   for i in range(0,len(suppr_explo)):
       suppr_explo[i] -= i
      
      
   for i in suppr_explo:
       temps_explo.pop(i)
       lieu_explo.pop(i)
       stade_explo.pop(i)
  
  
   suppr_explo = []

def test_niveau():
   #Fait varier les niveaux
   global level, augment,compte_niveau, hp, points
   dead = 0
   for i in liste_canons:
       if i.alive == False:
           dead += 1
   for i in liste_boss:
      if i.alive == False:
         dead += 1
   if augment == False and dead == 9:
       if (level+2)%3 == 0:
          hp += 5
       else:
          hp+=1
       if hp> hpmax:
          hp = int(hpmax)
       level += 1
       augment = True
       points += 50
       for i in liste_canons:
           i.xtir = []
           i.ytir = []
           i.xajout = []
           i.yajout = []
           i.liste_supression = []
       for i in liste_boss:
          i.xtir = []
          i.ytir = []
          i.xajout = []
          i.yajout = []
          i.liste_supression = []
           
   if augment == True:
           compte_niveau +=1
           if compte_niveau < 180:
               lvl = ecriture2.render("Niveau "+str(level-1)+":", True, (0,0,0))
               screen.blit(lvl, (500,400))
           else:
               compte_niveau = 0
               augment = False
               if (level-1)%9 == 0:
                  boss3.alive = True
                  canon1.alive = True
                  canon2.alive = True
                  canon5.alive = True
                  canon6.alive = True
                  boss3.hp = 6
                  canon1.hp = 3
                  canon2.hp = 3
                  canon5.hp = 3
                  canon6.hp = 3
               elif (level-1)%6 == 0:
                  boss2.alive = True
                  canon1.alive = True
                  canon2.alive = True
                  canon5.alive = True
                  canon6.alive = True
                  boss2.hp = 6
                  canon1.hp = 3
                  canon2.hp = 3
                  canon5.hp = 3
                  canon6.hp = 3
               elif (level-1)%3 == 0:
                  boss1.alive = True
                  canon1.alive = True
                  canon2.alive = True
                  canon5.alive = True
                  canon6.alive = True
                  boss1.hp = 15
                  canon1.hp = 3
                  canon2.hp = 3
                  canon5.hp = 3
                  canon6.hp = 3
               elif level == 2:
                  canon3.alive = True
                  canon4.alive = True
                  canon3.hp = 3
                  canon4.hp = 3
               elif level == 3:
                  canon3.alive = True
                  canon4.alive = True
                  canon2.alive = True
                  canon5.alive = True
                  canon3.hp = 3
                  canon4.hp = 3
                  canon2.hp = 3
                  canon5.hp = 3
               else:
                   for i in range(0,6):
                       liste_canons[i].alive = True
                       liste_canons[i].hp = 3
      
  
while running:
   #Boucle principale
   compteurs()
   test_niveau()
  
   if hp <= 0:
       lost = True
       gameover()
  
   for event in pygame.event.get():
       if event.type == pygame.QUIT:
           pygame.quit()
       if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
               pause()
   test_canons()
   test_boss()
   draw()
   test_explosions()
   test_niveau()
   keytest()
   pygame.display.flip()
   clock.tick(60)

pygame.quit()


